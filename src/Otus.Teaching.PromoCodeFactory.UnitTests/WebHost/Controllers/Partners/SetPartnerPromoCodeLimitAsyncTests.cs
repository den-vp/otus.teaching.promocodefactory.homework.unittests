﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        //TODO: Add Unit Tests
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly Mock<IRepository<PartnerPromoCodeLimit>> _PromoCodeLimitRepositoryMock;
        private readonly PartnersController _partnersController;

        /// <summary>
        /// Test for SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        /// </summary>
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _PromoCodeLimitRepositoryMock = fixture.Freeze<Mock<IRepository<PartnerPromoCodeLimit>>>();

            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };
            return partner;
        }

        [Fact] // 1. Если партнер не найден, то также нужно выдать ошибку 404;        
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_NotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            // setup repository for PartnerController
            Partner partner = null;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact] // 2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotActive_BadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.IsActive = false;

            // setup repository for PartnerController            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, null);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            Assert.Equal("Данный партнер не активен", (result as BadRequestObjectResult).Value);
        }

        // 3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes.
        // 4. При установке лимита нужно отключить предыдущий лимит;
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodesEqZeroIfHasActiveLimit_True()
        {
            // Arrange
            var NewPartnerId = Guid.Parse("a4f81b51-5b8f-44a5-8e9e-6802c2d9598c");
            Partner partner = PartnerBuilder.CreateBuilder()
                .WithId(NewPartnerId)
                .WithName("Partner Name")
                .WithIssuedPromoCodes(5)
                .WithActiveStatus(true);

            // partner has one active limit
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>() {
                new PartnerPromoCodeLimit() {
                    Id = Guid.NewGuid(),
                    PartnerId = NewPartnerId,
                    CreateDate = DateTime.Today.AddDays(-10),
                    EndDate = DateTime.Today.AddDays(-5),
                    CancelDate = null, // this limit is active.
                    Limit = 10
                }
            };

            // setup repository for PartnerController            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(NewPartnerId))
                .ReturnsAsync(partner);

            // make a new limit request with 
            SetPartnerPromoCodeLimitRequest newLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Today.AddDays(30),
                Limit = 15
            };

            // Act 1 When Limit is not finished, NumberIssuedPromoCodes should be zerofied. :)
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(NewPartnerId, newLimitRequest);

            result.Should().BeAssignableTo<CreatedAtActionResult>();

            // Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал 
            Assert.Equal(0, partner.NumberIssuedPromoCodes);

            // При установке лимита нужно отключить предыдущий лимит;
            List<PartnerPromoCodeLimit> limits = partner.PartnerLimits.OrderBy(l => l.CreateDate).ToList();
            if (limits.Count >= 2)
            {
                var prevLimit = limits[^2];
                Assert.True(prevLimit.CancelDate < DateTime.Now);
            }
        }

        [Fact] // 5. Лимит должен быть больше 0;
        public async void SetPartnerPromoCodeLimitAsync_NewLimitRequestMoreThanZero_True()
        {
            // Arrange
            var NewPartnerId = Guid.Parse("a4f81b51-5b8f-44a5-8e9e-6802c2d9598c");
            Partner partner = PartnerBuilder.CreateBuilder()
                .WithId(NewPartnerId)
                .WithName("Partner Name")
                .WithIssuedPromoCodes(5)
                .WithActiveStatus(true);

            // partner has one active limit
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>() {
                new PartnerPromoCodeLimit() {
                    Id = Guid.NewGuid(),
                    PartnerId = NewPartnerId,
                    CreateDate = DateTime.Today.AddDays(-10),
                    EndDate = DateTime.Today.AddDays(-5),
                    CancelDate = null, // this limit is active.
                    Limit = 10
                }
            };

            // setup repository for PartnerController            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(NewPartnerId))
                .ReturnsAsync(partner);

            // make a new limit request with Limit = -10
            SetPartnerPromoCodeLimitRequest newLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Today.AddDays(30),
                Limit = -10
            };

            // Act 
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(NewPartnerId, newLimitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
            Assert.Equal("Лимит должен быть больше 0", (result as BadRequestObjectResult).Value);
        }

        [Fact] // 6. Нужно убедиться, что сохранили новый лимит в базу данных
        public async void SetPartnerPromoCodeLimitAsync_NewLimitStoredSuccess_True()
        {
            // Arrange
            var NewPartnerId = Guid.Parse("a4f81b51-5b8f-44a5-8e9e-6802c2d9598c");
            Partner partner = PartnerBuilder.CreateBuilder()
                .WithId(NewPartnerId)
                .WithName("Partner Name")
                .WithIssuedPromoCodes(5)
                .WithActiveStatus(true);

            // partner has one active limit
            var LimitGuid = Guid.Parse("f402e72f-6a2c-4db1-b2e3-5c4e96c0c775");
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>() {
                new PartnerPromoCodeLimit() {
                    Id = LimitGuid,
                    PartnerId = NewPartnerId,
                    CreateDate = DateTime.Today.AddDays(-10),
                    EndDate = DateTime.Today.AddDays(-5),
                    CancelDate = null, // this limit is active.
                    Limit = 10
                }
            };

            // setup repository for PartnerController            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(NewPartnerId))
                .ReturnsAsync(partner);

            // make a new limit request with 
            SetPartnerPromoCodeLimitRequest newLimitRequest = new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = DateTime.Today.AddDays(30),
                Limit = 15
            };

            // act            
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(NewPartnerId, newLimitRequest);

            // assert
            var found = await _PromoCodeLimitRepositoryMock.Object.GetByIdAsync(LimitGuid);            
            Assert.NotNull(found);
        }
    }
}